import java.util.Scanner;
public class NationalPark{
	
	public static final Scanner scan = new Scanner(System.in);
	
	public static void main(String[] args)
	{
		TreeKanga[] mob = new TreeKanga[3]; //A group of Kangaroos is called "mob"
		
		//loop starts
		for (int i=0; i < mob.length; i++)
		{
			
			mob[i] = new TreeKanga(); 
		
			System.out.println("Please enter the name of your Tree Kangaroo");			
			mob[i].name = scan.next();
			System.out.println("Please enter the size (in cm) of your Tree Kangaroo");
			mob[i].size = scan.nextInt();
			System.out.println("Please enter the gender (M or F) of your Tree Kangaroo"); //Assuming the user will only use the char in capital letters
			mob[i].gender = scan.next().charAt(0);
			System.out.println("Please enter the color of your Tree Kangaroo");
			mob[i].color = scan.next();
			System.out.println();
			
			
		}
		
		
		System.out.println("The last Kangaroo entered was:  "+mob[2].name +", " + mob[2].size + ", " + mob[2].gender + ", " + mob[2].color);
		
	
		mob[0].IsFertile();
		mob[0].ClimbTree();
		mob[0].CarryBaby();
	}
	
}